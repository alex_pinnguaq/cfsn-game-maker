﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RPGM.Core;
using RPGM.Gameplay;
using RPGM.UI;
using UnityEngine.Events;

public class CusceneManager : MonoBehaviour
{

    public GameObject m_postSceneTarget; // This should be the game object over which we center the camera post-cutscene.
    public List<GameObject> m_slates; 
    public int m_curSlate;

    bool m_cutsceneActive = false;

    GameObject m_activeSlate;

    public CameraManager m_cameraManager;

    GameModel model = Schedule.GetModel<GameModel>();

    public UnityEvent m_onCutsceneComplete;

    // Start is called before the first frame update
    void Start()
    {
        m_cameraManager = GameObject.FindObjectOfType<CameraManager>();
        Reset();
    }

    public void Reset()
    {
        m_curSlate = 0;
        if( m_slates.Count > 0){
            m_activeSlate = m_slates[0];
            m_activeSlate.SetActive(true);
            for( int i = 1; i < m_slates.Count; i++){
                m_slates[i].SetActive(false);
            }
        }
        
    }

    public void StartCutscene(){
        m_cutsceneActive = true;
        m_cameraManager.SnapToObject( gameObject);
        model.input.ChangeState(InputController.State.Pause);
    }

    // Update is called once per frame
    void Update()
    {
        if (m_cutsceneActive && Input.GetKeyDown(KeyCode.Space)){
            m_curSlate += 1;
            if( m_curSlate >= m_slates.Count){
                CompleteCutscene();
            }
            else
            {
                m_activeSlate.SetActive(false);
                m_activeSlate =  m_slates[m_curSlate];
                m_activeSlate.SetActive(true);
                
            }
        }
    }

    public void CompleteCutscene(){
        m_cutsceneActive = false;
        m_cameraManager.SnapToObject( m_postSceneTarget);
        model.input.ChangeState(InputController.State.CharacterControl);
        m_onCutsceneComplete.Invoke();
    }
}
