﻿using RPGM.UI;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class GameRestart : MonoBehaviour
{
    public UnityEvent m_onRestart;
    public InputController m_inputController;
    public GameObject m_playerObject;
    public GameObject m_startPos;

    public void Restart()
    {
        m_onRestart.Invoke();
        m_playerObject.transform.position = m_startPos.transform.position;
    }

    public void SetPlayerLocked(bool locked)
    {
        if (locked)
        {
            m_inputController.ChangeState(InputController.State.Pause);
        }
        else
        {
            m_inputController.ChangeState(InputController.State.CharacterControl);
        }
    }
}
