﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using RPGM.Gameplay;

public class SceneWarper : MonoBehaviour
{
    public SceneWarper m_targetWarper;
    public GameObject m_visitorDropPoint;
    public UnityEvent m_onTeleportedTo; 

    public CameraManager m_cameraManager;

    // Start is called before the first frame update
    void Start()
    {
        m_cameraManager = GameObject.FindObjectOfType<CameraManager>();
    }

    void OnTriggerEnter2D(Collider2D collider)
    {
        DoWarp(collider.gameObject);
    }

    public void DoWarp(GameObject warpObject)
    {
        if (m_targetWarper != null)
        {
            // if (warpObject != null)
            // {
            //     warpObject.transform.position = m_targetWarper.m_visitorDropPoint.transform.position;
            // }
            // m_cameraManager.SnapToObject(m_targetWarper.transform.parent.gameObject);
            // Vector3 targetCamPos = m_targetWarper.transform.parent.transform.position;
            // targetCamPos.z = -10;
            // GameObject.Find("Main Camera").transform.position = targetCamPos;

            Warp( warpObject,m_targetWarper.m_visitorDropPoint, m_targetWarper.transform.parent.gameObject );

            m_targetWarper.m_onTeleportedTo.Invoke();

        }
    }

    void Warp(GameObject obj, GameObject warpTarget, GameObject camPosition){
        if( obj != null && warpTarget != null){
            obj.transform.position = warpTarget.transform.position;
        }
        m_cameraManager.SnapToObject( camPosition);
    }

    public void WarpPlayerToMe(){
        CharacterController2D player = GameObject.FindObjectOfType<CharacterController2D>();
        print( player.gameObject);
        print( m_visitorDropPoint);
        print( transform.parent.gameObject);
        Warp(player.gameObject, m_visitorDropPoint, transform.parent.gameObject);
    }
    

    // Update is called once per frame
    void Update()
    {
        
    }
}


