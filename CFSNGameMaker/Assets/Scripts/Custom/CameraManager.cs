﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CameraManager : MonoBehaviour
{
    public GameObject m_playerController;
    public GameObject m_camera;
    public bool m_startOnPlayer;

    public GameObject m_introAnchor;
    public GameObject m_outroAnchor;
    public GameObject m_startAnchor;

    CameraAnchor[] m_anchors;


    public void SnapIntro()
    {
        SnapCameraPos(m_introAnchor.transform.position);
    }

    public void SnapOutro()
    {
        SnapCameraPos(m_outroAnchor.transform.position);

    }

    public void SnapToStart()
    {
        SnapCameraPos(m_startAnchor.transform.position);
    }

    public void SnapToObject(GameObject gameObject){
        SnapCameraPos( gameObject.transform.position);
    }

    void SnapCameraPos(Vector3 newPos)
    {
        m_camera.transform.position = new Vector3(newPos[0], newPos[1], m_camera.transform.position[2]);
    }

    public void SnapToPlayer()
    {
        m_anchors = FindObjectsOfType<CameraAnchor>();
        Vector3 closestAnchorPos = new Vector3(0, 0, 0);
        Vector3 playerPos = m_playerController.transform.position;

        float closestDist = float.MaxValue;
        for (int i = 0; i < m_anchors.Length; i++)
        {
            float anchorDist = Vector3.Distance(m_anchors[i].transform.position, playerPos);
            if (anchorDist < closestDist)
            {
                closestDist = anchorDist;
                closestAnchorPos = m_anchors[i].transform.position;
            }
        }
        SnapCameraPos(closestAnchorPos);
    }

}
