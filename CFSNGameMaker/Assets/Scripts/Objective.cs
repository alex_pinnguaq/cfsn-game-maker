using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
public class Objective : MonoBehaviour
{
    
    public enum EObjectiveType {Single, Sequence, And, Or};

    public EObjectiveType m_type;
    public bool m_active = false; // Is this objective active? In other words, will triggering "Complete do anything?"
    public bool m_complete = false;
    public UnityEvent m_onStart;
    public UnityEvent m_onComplete;
    public int m_sequence;

    Objective m_parent;
    List<Objective> m_childObjectives = new List<Objective>();
    
    // Start is called before the first frame update
    void Start()
    {
        m_parent = transform.parent.gameObject.GetComponent( typeof(Objective)) as Objective;
        Objective[] obs = FindObjectsOfType<Objective>();
        foreach( Objective ob in obs){
            m_childObjectives.Add(ob);
        }
        if( m_type == EObjectiveType.Sequence){
            m_childObjectives.Sort( delegate(Objective o1, Objective o2){
                if( o1.m_sequence > o2.m_sequence){
                    return 1;
                }
                return -1;
            });
        }
    }
    
    public void Restart(){
        m_complete = false;
        m_active = false;

        foreach (Objective ob in m_childObjectives)
        {
            ob.Restart();
        }
    }

    public void Begin(){
        if( m_active == true){
            return;
        }
        m_onStart.Invoke();
        m_active = true;
        if( m_childObjectives.Count > 0){
            if( m_type == EObjectiveType.Sequence){
                m_childObjectives[0].Begin();
            }
            else{
                foreach (Objective ob in m_childObjectives)
                {
                    ob.Begin();
                }
            }
        }
    }

    // This will be called by a child objective, letting it's parent know that it's complete.
    // This should never be called unless the child object is active
    public void OnChildObjectiveComplete(Objective completeObjective){
        if( !m_childObjectives.Contains(completeObjective)){
            Debug.Log("Child objective triggered but not found in sequence");
            return;
        }
        bool complete = (m_type == EObjectiveType.Or);
        if( m_type == EObjectiveType.And){
            foreach( Objective childO in m_childObjectives){
                if( !childO.m_complete){
                    complete = false;
                    break;
                }
            }    
        }
        else if( m_type == EObjectiveType.Sequence){
            int i = 0;
            for(; i < m_childObjectives.Count; i++){
                if( m_childObjectives[i] == completeObjective){
                    break;
                }
            }

            if (i == m_childObjectives.Count - 1){
                complete = true;
            }
            else{
                m_childObjectives[i].Start();
            }
        }

        if( complete ){
            DoComplete();
        }
    }

    void DisableChildren(){
        foreach( Objective childO in m_childObjectives){
            childO.m_active = false;
        }
    }

    // Mark this objective as complete.  This should only propogate it's information if it's active and not already complete
    public void DoComplete(){
        if( m_active == false || m_complete == true){
            return;
        }
        m_onComplete.Invoke();
        if( m_parent != null){
            m_parent.OnChildObjectiveComplete(this);
        }
    }
}
