﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnowballKid : MonoBehaviour
{
    public enum EThrowState{
        Idle,
        Throwing,
    }

    public GameObject m_snowballPrefab;
    public GameObject m_standing;
    public GameObject m_throwing;
    public EThrowState m_throwState;
    public float m_throwFrequency;
    public float m_throwFrameTime;
    public Vector2 m_throwVelocity;
    public float m_snowballLife;
    float m_curThrowTime = 0;
    // Start is called before the first frame update
    void Start()
    {
        SetThrowState(m_throwState);
    }

    // Update is called once per frame
    void Update()
    {
        if( m_throwState == EThrowState.Throwing){
            m_curThrowTime += Time.deltaTime;
            if( m_curThrowTime > m_throwFrequency + m_throwFrameTime){
                m_curThrowTime = 0;
                SetSprite(false);
            }
            else if( m_curThrowTime > m_throwFrequency){
                SetSprite(true);
                if( m_curThrowTime - Time.deltaTime <= m_throwFrequency){
                    SpawnSnowball();
                }
            }
        }
    }

    void SpawnSnowball(){
        GameObject snowball = Instantiate( m_snowballPrefab, gameObject.transform.position, Quaternion.identity);
        Snowball s = snowball.GetComponent<Snowball>();
        s.m_lifeLength = m_snowballLife;
        s.m_velocity = m_throwVelocity;

    }

    public void SetThrowState(EThrowState throwState ){
        m_throwState = throwState;
        SetSprite(false);
        m_curThrowTime = 0;
    }

    public void SetThrowing(bool isThrowing){
        if( isThrowing){
            SetThrowState(EThrowState.Throwing);
        }
        else{
            SetThrowState(EThrowState.Idle);
        }
    }

    void SetSprite( bool throwing){
        
        m_standing.gameObject.SetActive( !throwing);
        m_throwing.gameObject.SetActive(throwing);
    }
}
