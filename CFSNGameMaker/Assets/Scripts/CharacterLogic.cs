﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using RPGM.Core;
using RPGM.Gameplay;

public class CharacterLogic : MonoBehaviour
{

    public NPCController m_npcController;
    public ConversationScript m_firstConvo;


    // Start is called before the first frame update
    void Start()
    {
        m_npcController.TriggerConversation(m_firstConvo);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}
