using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;


public class ObjectiveManager : Objective
{

    // Update is called once per frame
    void Update()
    {
        
    }

    public void StartObjectives(){
        
    }



    public CameraManager m_camManager;
    public GameRestart m_restartScript;
    public bool m_debugStart;

    // Start is called before the first frame update
    void Start()
    {
        Reset();
    }

    public void Reset()
    {
        if (m_debugStart)
        {
            DoDebugStart();
        }
        else{
            DoStart();
        }
    
    }


    void DoStart()
    {
        m_restartScript.Restart();
        m_camManager.SnapToStart();
    }

    void DoDebugStart()
    {
        m_camManager.SnapToPlayer();
    }
}
