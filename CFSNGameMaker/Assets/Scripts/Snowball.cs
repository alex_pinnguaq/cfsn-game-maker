﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Snowball : MonoBehaviour
{
    public float m_lifeLength = 1;
    public float m_curTime = 0;
    public Vector2 m_velocity;
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        m_curTime += Time.deltaTime;
        if( m_curTime >= m_lifeLength){
            Destroy(gameObject);
        }

        transform.position+= new Vector3( m_velocity[0] * Time.deltaTime, m_velocity[1] * Time.deltaTime, 0);

    }
}
